
分項 1 & 3 : 系統規劃與視覺化工具開發

HackMD : https://hackmd.io/PXW837CuTK2yeH2BxXX2qQ

# Usage :
## open annotator
1. `npm install`
2. `npm run dev`
3. go to <http://localhost:8080/>
4. install [react-developer-tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=zh-TW) for better debug tool

## open local firebase cloud function
1. `cd firebase/functions`
2. `npm install`
2. `npm run serve`

## Testcase :
1. go to FTP to download files

## Files

- lib: some library 
- src: your code
    - view: web page view
    - component: react component
    - data: get/save data
- data: some test data json, video ...

# Tips(???)

## After git Pull
Remember to run `npm install` again if getting import error.

## Test without login
```javascript
// comment this without re-entering account again
{!this.props.auth.isLogin()
    && <Redirect to="/login"/>
}
```

## Coding and Naming style???

- ThisIsAJavaScriptFile.js
- `class` ThisIsAReactComponent `extend React.Component`
- **2 Spaces Indent** for component. 