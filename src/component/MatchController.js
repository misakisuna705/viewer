import PropTypes from 'prop-types';
import { withStyles, AppBar, Tabs, Tab } from '@material-ui/core';
import GameTable from './match/gameTable'
import PlayerTable from './match/playerTable'

const styles = theme => ({
  containerStyle: {
    height: '100%',
    padding: '20px'
  },
  treeStyle: {
    flex: 1,
  },
  barStyle: {
    margin: '30px 0px 0px 0px'
  }
});
  

class MatchController extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    };
  }

  handleChange = (event, value) => {
    this.setState({value});
  }
  
  render() {
    const {classes} = this.props;
    const {value} = this.state;

    return (
    <div className="matchcontroller">
      <AppBar position="static" color="default" className={classes.barStyle}>
        <Tabs 
          value={value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          centered
          aria-label="tab"
        >
          <Tab label="Game List" />
          <Tab label="Player List" />
        </Tabs>
      </AppBar>
      {value === 0 && <GameTable />}
      {value === 1 && <PlayerTable />}
    </div>
    );
  }
}

MatchController.propTypes = {
  classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(MatchController);
  