import { Grid, Typography, withStyles, Button, TextField } from "@material-ui/core";
import ThreeController from "./video/threeController";
import VideoFrame from "./video/VideoFrame";
import PlaybackController from "./video/playbackController";

function onNextFrame(callback) {
  setTimeout(() => {
    requestAnimationFrame(callback);
  });
}

const useStyles = (theme) => ({
  root: {
    backgroundColor: "#A8A8A8",
  },
  title: {
    textAlign: "center",
  },
  body: {
    position: "relative",
    backgroundColor: "#C8C8C8",
  },
});

class VideoController extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dimension: {
        width: 640,
        height: 360,
        ratio: 0.5625,
      },
      game: "Fan_Zhendong_vs_Jeoung_Youngsik_2019_ITTF_Korea_14",
      video: {
        dimension: {
          width: 1280,
          height: 720,
        },
        folder: "/data/video/boxing1.mp4",
        type: ".mp4",
        // path:
        //   "/data/video/Fan_Zhendong_vs_Jeoung_Youngsik_2019_ITTF_Korea_14.mp4",
        path: "data/video",
      },
      label: {
        folder: "/data/label/",
        type: "ball_label.json",
        path:
          "/data/label/Fan_Zhendong_vs_Jeoung_Youngsik_2019_ITTF_Korea_14/ball_label.json",
      },
      pose: {
        folder: "/data/label/",
        type: "human_tracking_label.json",
        path: "/data/label/post_raw2D.txt",
        is2D_3D: "2D",
        isIK_raw: "raw",
        poseType: "xnect"
      },
      pose3D: {
        folder: "/data/label/",
        path: "/data/label/post_raw3D.txt",
        is2D_3D: "3D",
        isIK_raw: "raw",
        poseType: "xnect"
      },
      vid: null,
      vidFrame: null,
    };

    this.updateLabel = this.updateLabel.bind(this);
  }

  componentDidMount() {


    this.event = [
      {
        value: 100,
        label: "event1",
      },
    ];

    this.video = document.createElement("video");
    this.video.id = this.state.game;
    this.video.onloadeddata = (() => {
      this.frame = new VideoFrame({
        vid: this.video,
        frameRate: 29.97,
        callback: this.updateLabel,
      });
      this.frame.listen("frame");

      this.setState({
        dimension: {
          width: this.videoRoot.offsetWidth,
          height: this.videoRoot.offsetWidth * this.state.dimension.ratio,
        },
        vid: this.video,
        vidFrame: this.frame,
      });
    }).bind(this);
    this.video.src = "/data/video/boxing1.mp4";

  }

  updateLabel(frame) {
    this.threeRef.updatePose(frame);
  }

  save3DPose ()
  {
    // --- get fileData --- //
    let fileData;
    this.pose3D.poseMap.forEach((val, frame)=>{
      for (let humanID in val)
      {
        const pose = val[humanID]
        let line_to_write = frame.toString();
        line_to_write = line_to_write + " " + humanID;
        pose["keypoints"].forEach((coord)=>{
          line_to_write = line_to_write + " " + coord.cor_x + " " + coord.cor_y + " " + coord.cor_z;
        })
        line_to_write += "\n"
        fileData += line_to_write;
      }
    })

    // --- download the file --- //
    const blob = new Blob([fileData], {type: "text/plain"});
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.download = 'new3DPose.txt';
    link.href = url;
    link.click();
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container alignItems="center" spacing={1}>
          <Grid item xs={2}>
            <Typography variant="h6" className={classes.title}>
              Title
            </Typography>
          </Grid>
        </Grid>
        <div
          ref={(videoRoot) => {
            this.videoRoot = videoRoot;
          }}
          className={classes.body}
        >
          {this.state.vid && (
            <ThreeController
              ref={(threeRef) => {
                this.threeRef = threeRef;
              }}
              dimension={this.state.dimension}
              videoDimension={this.state.video.dimension}
              vid={this.state.vid}
              pose3DPath={this.state.pose3D}
              pose2DPath={this.state.pose}
            />
          )}
          {this.state.vid && (
            <PlaybackController
              marks={this.event}
              vid={this.state.vid}
              vidFrame={this.state.vidFrame}
              updateCallback={this.updateLabel}
            />
          )}
          {
            <Grid container justify="center" spacing={1}>
              <Grid item xs={2}>
                <Button
                  fullWidth
                  onClick={() => {
                    this.threeRef.toggle2D();
                  }}
                >
                  toggle 2D Pose
                </Button>
              </Grid>
              <Grid>
                <Button
                  fullWidth
                  onClick={() => {
                    this.threeRef.toggle3DPose();
                  }}
                >
                  toggle 3D pose
                </Button>
              </Grid>
              <Grid>
                <Button
                  fullWidth
                  onClick={() => {
                    this.save3DPose();
                  }}
                >
                  save 3D pose
                </Button>
              </Grid>
              <Grid item xs={2}>
                <Button
                  fullWidth
                  onClick={() => {
                    this.threeRef.toggleMode();
                  }}
                >
                  mode
                </Button>
              </Grid>
              <Grid>
                <TextField 
                label="newID"
                id="newID"></TextField>
              </Grid>

              <Grid>
                <Button
                  onClick={()=>{
                    const newID = document.getElementById("newID").value
                    this.threeRef.pose3D.setNewID(newID)
                  }}
                >
                  set new ID
                </Button>
              </Grid>

              <Grid>
                <Button
                  onClick={()=>{
                    this.threeRef.pose3D.removeSelectedID("current")
                  }}
                >
                  remove selected ID
                </Button>
              </Grid>

              <Grid>
                <Button
                  onClick={()=>{
                    this.threeRef.pose3D.removeSelectedID("after")
                  }}
                >
                  remove selected ID(all the frames after)
                </Button>
              </Grid>

              <Grid>
                <Button
                  onClick={()=>{
                    this.threeRef.pose3D.removeSelectedID("all")
                  }}
                >
                  remove selected ID(all frames)
                </Button>
              </Grid>



            </Grid>
          }
        </div>
      </div>
    );
  }
}

export default withStyles(useStyles)(VideoController);
