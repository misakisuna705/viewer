import { withStyles, makeStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = (theme) => ({
  multiButton:{
    paddingBottom: theme.spacing(2),
  },
  selectTitle:{
    marginRight: theme.spacing(2),
    display: "inline-block"
  },
  selectButton:{
    marginRight: theme.spacing(1),
  }
});

class _MultiButton extends React.Component {
  constructor(props) {
    super(props);

    let colors = [];
    for(let i=0; i<props.options.length; i++){
      colors.push("default")
    }
    console.log(colors);
    this.state = {
      value: null,
      colors: colors
    }
  }

  clickButton(index){
    this.setState((state, props) => {
      const colors = state.colors.map((item, j) => {
        if (j === index) {
          return "primary";
        } else {
          return "default";
        }
      });
      return {
        colors: colors,
        value: props.options[index]
      };
    });
  }

  render() {
    const { classes } = this.props;
    return (
    <div className={classes.multiButton}>
      {"title" in this.props ? 
        <Typography className={classes.selectTitle} 
                    variant="body1" color="textPrimary" gutterBottom>
          {this.props.title}
        </Typography>
        : null}
      {this.props.options.map((value, index) => {
        return (
          <Button key={index} className={classes.selectButton} 
                  variant="contained" size="small" 
                  color={this.state.colors[index]}
                  onClick={() => { this.clickButton(index) }}> 
            {value} 
          </Button>
        )
      })}
    </div>
  )
  }
}
const MultiButton = withStyles(useStyles)(_MultiButton);

export default class SingleSelecter extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      value: null
    }
  }
  render() {
    return (
      <div>
        {this.props.options.options.map((value, index) => {
          return <MultiButton key={index}options={value.tags} title={value.title}/>
        })}
      </div>
    );
  }
}