import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import ScoreInputForm from './ScoreInputForm'
import ActionInputForm from './ActionInputForm'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    height:'95%',
    backgroundColor: theme.palette.background.paper,
    overflowY: 'scroll',
  },
});
const divStyle={
  float: 'left',
  height:'615px',
  width: '100%',
  position:'relative'
};

class ScrollableTabsButtonAuto extends React.Component {
  render() {
    const { classes } = this.props;
    const { value } = this.props.eventInfos;
    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.props.handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            // scrollable
            scrollButtons="auto"
            centered
          >
            <Tab label="動作" />
            <Tab label="得分" />
          </Tabs>
        </AppBar>
        <div style={divStyle}>
          {value === 0 && <TabContainer>
                              <ActionInputForm 
                                handleEditor={this.props.handleEditor} 
                                eventInfos={this.props.eventInfos}/>
                          </TabContainer>}
          {value === 1 && <TabContainer>
                              <ScoreInputForm handleForm={this.props.handleForm}/>
                          </TabContainer>}
        </div>
      </div>
    );
  }
}

ScrollableTabsButtonAuto.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ScrollableTabsButtonAuto);
