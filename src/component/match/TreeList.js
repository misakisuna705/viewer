import {Select, FormControl, FormHelperText, MenuItem, NativeSelect, InputLabel, withStyles } from '@material-ui/core'
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import PropTypes from 'prop-types';

const styles = theme => ({
    selectStyle: {
        margin: '30px'
    },
    treeItemStyle: {
        width: '200px'
    }
});

const game1 = {
    id: '3',
    name: 'Game 1',
    children: [
        {
            id: '5',
            name: 'first period'
        },
        {
            id: '6',
            name: 'second period'
        },
        {
            id: '7',
            name: 'third period'
        },
        {
            id: '8',
            name: 'fourth period'
        }
    ]
}
const game2 = {
    id: '4',
    name: 'Game 2'
}
const dataBasedOnPlayer = {
    id: 'root',
    name: "Based on player",
    children: [
        {
            id: '1',
            name: "Player A",
            children: [
                game1,
                game2
            ]
        },
        {
            id: '2',
            name: "player B",
            children: [
                game1,
                game2
            ]
        }
    ]
};
const dataBasedOnGame = {
    id: 'root',
    name: "Based on game",
    children: [
        game1,
        game2
    ]
};

class TreeList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            based: 0,
            selectedNodeId: 'root'
        };
    }

    renderTree = (nodes) => (
        <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.name}>
            {Array.isArray(nodes.children) ? nodes.children.map((node) => this.renderTree(node)) : null}
        </TreeItem>
    );

    handleChange = (event) => {
        console.log(event.target.value)
        this.setState({
            based: event.target.value
        });
    };

    handleSelect = (event, nodeId) => {
        console.log(`nodeId = ${nodeId}`);
        this.setState({
            selectedNodeId: nodeId
        });
    };

    render() {
        const {classes} = this.props;

        return (
        <div>
            <FormControl
            fullWidth={true}
            >
                <InputLabel>
                    Based on
                </InputLabel>
                <NativeSelect
                className={classes.selectStyle}
                value={this.state.based}
                onChange={this.handleChange}
                name="based"
                >
                    <option value={0}>Player</option>
                    <option value={1}>Game</option>
                </NativeSelect>
                <FormHelperText></FormHelperText>
            </FormControl>
            <TreeView
            className={classes.treeItemStyle}
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpandIcon={<ChevronRightIcon />}
            onNodeSelect={this.handleSelect}
            >
                {(this.state.based == 0) ? this.renderTree(dataBasedOnPlayer) : this.renderTree(dataBasedOnGame)}
            </TreeView>
        </div>
        );
    }
}

TreeList.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(TreeList);