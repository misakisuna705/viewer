import PropTypes from 'prop-types';
import { withStyles, Grid, Paper, Box, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Button, FormControl, InputLabel, Select, MenuItem, Container, Typography, makeStyles } from '@material-ui/core/';
import MaterialTable from "material-table";

import DeleteOutline from '@material-ui/icons/DeleteOutline';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import LabelIcon from '@material-ui/icons/Label';
import Global from '../../data/Global';

const styles = theme => ({
  input: {
    display: 'none',
  },
  border: {
    border: '5px red solid'
  },
  scoreTextField: {
    width: '120px',
    margin: '5px 10px 0px 10px'
  },
  uploadVideoData: {
    minHeight: '450px',
  },
  uploadVideoDescription: {
    height: '150px',
  },
  uploadVideoScore: {
    minHeight: '300px',
  },
  paper: {
    'border-right': '1px black solid'
  }
});

class GameTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      videoFile: '',
      matchName: '',
      matchData: {
        team1: {
          name: '',
          playerNumber: '',
          playerList:[]
        },
        team2: {
          name: '',
          playerNumber: '',
          playerList:[]
        },
        set: {  // TODO : inherit from sport type
          score: {
            team1: -1,
            team2: -1
          },
          set: {
            score: null
          }
        }
      },
      matchlist: []
    };
  }

  handleOpen = () => {
    console.log("open");
    this.setState({ open: true });
  }

  handleClose = () => {
    console.log("close");
    this.setState({ open: false });
  }

  handleUpload = () => {
    console.log("upload");
    this.setState({ open: false });
  }

  handleFileChange = ({ target }) => {
    const files = target.files;
    if (files.length === 0) return;

    this.setState({ videoName: files[0] });
  }

  handleNameChange = (event) => {
    this.setState({
      matchName: event.target.value
    });
  }

  handleTeam1NameChange = (event) => {
    let oriMatchData = this.state.matchData
    oriMatchData.team1.name = event.target.value
    this.setState({
      matchData: oriMatchData
    });
  }

  handleTeam2NameChange = (event) => {
    let oriMatchData = this.state.matchData
    oriMatchData.team2.name = event.target.value
    this.setState({
      matchData: oriMatchData
    });
  }

  handleTeam1NumberChange = (event) => {  // TODO : 擴展 PlayerSelect
    let oriMatchData = this.state.matchData
    oriMatchData.team1.playerNumber = event.target.value

    let playerlist = []
    let playernumber = parseInt(event.target.value)
    if (playernumber > 0) {
      for (var i = 0; i < playernumber; i++) {
        playerlist.push('')
      }
    }
    oriMatchData.team1.playerList = playerlist

    this.setState({
      matchData: oriMatchData
    });
  }

  handleTeam2NumberChange = (event) => {
    let oriMatchData = this.state.matchData
    oriMatchData.team2.playerNumber = event.target.value

    let playerlist = []
    let playernumber = parseInt(event.target.value)
    if (playernumber > 0) {
      for (var i = 0; i < playernumber; i++) {
        playerlist.push('')
      }
    }
    oriMatchData.team2.playerList = playerlist

    this.setState({
      matchData: oriMatchData
    });
  }

  render() {
    const { classes } = this.props;
    const { open } = this.state;
    console.log(this.state)

    return (
      <div>
        <MaterialTable
          title="比賽列表"
          columns={[
            { title: "上傳時間", field: "uploadTime" },
            { title: "比賽名稱", field: "gameTitle" },
            { title: "選手", field: "players" },
            { title: "結果", field: "gameResult" },
          ]}
          data={[
            {
              uploadTime: "2018/20/20 10:10:10",
              gameTitle: "2018德國公開賽",
              players: "樊振東 VS 馬龍",
              gameResult: "4:2"
            },
            {
              uploadTime: "2018/20/20 10:10:10",
              gameTitle: "2018德國公開賽",
              players: "樊振東 VS 馬龍",
              gameResult: "1:2"
            },
            {
              uploadTime: "2018/20/20 10:10:10",
              gameTitle: "2018德國公開賽",
              players: "樊振東 VS 馬龍",
              gameResult: "5:6"
            },
          ]}
          actions={[
            {
              icon: () => <CloudUploadIcon />,
              tooltip: 'Upload Video',
              isFreeAction: true,
              onClick: (event) => { this.handleOpen(); }
            },
            {
              icon: () => <LabelIcon />,
              tooltip: 'Label Video',
              onClick: (event) => alert("Label video!~")
            },
            {
              icon: () => <DeleteOutline />,
              tooltip: 'Delete Video',
              onClick: (event, rowData) => alert("Delete video!~")
            }
          ]}
          detailPanel={rowData => {
            // console.log(rowData);
            return (
              <div>
                <Box margin={1}>
                  <Grid container spacing={3} >
                    <Grid container item xs={12} justify="flex-start" alignItems='center' align='center'>
                      <Grid item xs={1}><Typography variant="body1">比賽名稱</Typography></Grid>
                      <Grid item xs={3} align='center'><Paper><TextField inputProps={{ style: { textAlign: 'center' } }} size="small" defaultValue="2018德國公開賽" /></Paper></Grid>
                    </Grid>

                    <Grid container item xs={12} align='center'>
                      <Grid item xs={1}><Typography variant="body1">選手</Typography></Grid>
                      <Grid item xs={1} align='center'><Paper><TextField inputProps={{ style: { textAlign: 'center' } }} size="small" defaultValue="player1" /></Paper></Grid>
                      <Grid item xs={1} align='center'>VS</Grid>
                      <Grid item xs={1} align='center'><Paper><TextField inputProps={{ style: { textAlign: 'center' } }} size="small" defaultValue="player2" /></Paper></Grid>
                    </Grid>

                    <Grid container item xs={12} align='center'>
                      <Grid item xs={1}><Typography variant="body1">比分</Typography></Grid>
                      <Grid item xs={1} align='center'><Paper><TextField inputProps={{ style: { textAlign: 'center' } }} size="small" type="number" defaultValue="5" /></Paper></Grid>
                      <Grid item xs={1} align='center'>:</Grid>
                      <Grid item xs={1} align='center'><Paper><TextField inputProps={{ style: { textAlign: 'center' } }} size="small" type="number" defaultValue="4" /></Paper></Grid>
                    </Grid>

                    <Grid container item xs={12} align='center'>
                      <Grid item xs={1}><Typography variant="body1">局分</Typography></Grid>
                      <Grid item xs={1} align='center'><Paper><TextField inputProps={{ style: { textAlign: 'center' } }} size="small" type="number" defaultValue="11" /></Paper></Grid>
                      <Grid item xs={1} align='center'>:</Grid>
                      <Grid item xs={1} align='center'><Paper><TextField inputProps={{ style: { textAlign: 'center' } }} size="small" type="number" defaultValue="9" /></Paper></Grid>
                    </Grid>

                    <Grid item xs={12} align='right'>
                      <Button variant="contained" color="primary">Save</Button>
                    </Grid>

                  </Grid>
                </Box>
              </div>
            )
          }}
          options={{
            actionsColumnIndex: -1
          }}
        />
        <Dialog open={open} onClose={this.handleClose} maxWidth='md'>
          <DialogTitle>上傳比賽資料</DialogTitle>
          <DialogContent>
            <Grid container className={classes.uploadVideoData} spacing={6}>
              <Grid container item xs={6} direction="column">
                <Grid item className={classes.uploadVideoDescription}>
                  <input
                    className={classes.input}
                    onChange={this.handleFileChange}
                    accept="video/*"
                    id="upload-video-file"
                    type="file"
                  />
                  <label htmlFor="upload-video-file">
                    <Button variant="contained" color="primary" component="span">
                      Upload Video
                    </Button>
                  </label>
                  <TextField id="videoName" label="影片名稱" margin="dense" fullWidth onChange={this.handleNameChange} value={this.state.matchName} />
                </Grid>
                <Grid container item xs direction="column" className={classes.uploadVideoScore}>
                  <Grid container item xs>
                    <Grid item xs={3} align="center">
                      <Typography variant="body1">最終比分</Typography>
                    </Grid>
                    <Grid item xs={9} align="center">
                      <TextField id="Team1FinalScore" variant="standard" label="Team1" size="small" type="number" InputLabelProps={{ shrink: true, }} className={classes.scoreTextField} />
                      <TextField id="Team2FinalScore" variant="standard" label="Team2" size="small" type="number" InputLabelProps={{ shrink: true, }} className={classes.scoreTextField} />
                    </Grid>
                  </Grid>
                  <Grid container item xs>
                    <Grid item xs={3} align="center">
                      <Typography variant="body1">各局比分</Typography>
                    </Grid>
                    <Grid item xs={9} align="center">
                      <TextField id="Team1FirstRoundScore" variant="standard" label="Team1" size="small" type="number" InputLabelProps={{ shrink: true, }} className={classes.scoreTextField} />
                      <TextField id="Team2FirstRoundScore" variant="standard" label="Team2" size="small" type="number" InputLabelProps={{ shrink: true, }} className={classes.scoreTextField} />
                      <TextField id="Team1SecondRoundScore" variant="standard" label="Team1" size="small" type="number" InputLabelProps={{ shrink: true, }} className={classes.scoreTextField} />
                      <TextField id="Team2SecondRoundScore" variant="standard" label="Team2" size="small" type="number" InputLabelProps={{ shrink: true, }} className={classes.scoreTextField} />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid container item xs={3} direction="column" className={classes.paper}>
                <Grid item xs>
                  <Typography variant="body1">Team1</Typography>
                  <TextField id="Team1Name" label="名稱" margin="dense" onChange={this.handleTeam1NameChange} value={this.state.matchData.team1.name} />
                  <TextField id="Team1playerNum" label="人數" margin="dense" onChange={this.handleTeam1NumberChange} value={this.state.matchData.team1.playerNumber} />
                </Grid>
                <Grid item xs>
                  <Typography variant="body2">選手列表</Typography>
                  {
                    // 擴展 PlayerSelect
                    this.state.matchData.team1.playerList.map((data,index) => {
                      return (<PlayerSelect key={index}/>)
                    })
                  }
                </Grid>
              </Grid>
              <Grid container item xs={3} direction="column">
                <Grid item xs>
                  <Typography variant="body1">Team2</Typography>
                  <TextField id="Team2Name" label="名稱" margin="dense" onChange={this.handleTeam2NameChange} value={this.state.matchData.team2.name} />
                  <TextField id="Team2playerNum" label="人數" margin="dense" onChange={this.handleTeam2NumberChange} value={this.state.matchData.team2.playerNumber} />
                </Grid>
                <Grid item xs>
                  <Typography variant="body2">選手列表</Typography>
                  {
                    // 擴展 PlayerSelect
                    this.state.matchData.team2.playerList.map((data,index) => {
                      return (<PlayerSelect key={index}/>)
                    })
                  }
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="secondary">
              Cancel
            </Button>
            <Button onClick={this.handleUpload} color="primary">
              Upload
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }

  componentDidMount() {
    this.getMatchList()
  }

  getMatchList() {
    Global.Instance.apicenter.getMatchList().then(data => {
      console.log(data)
      this.setState({
        matchlist: data
      })
    });
  }
}

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  }
}));

const playerlist = [
  "樊振東",
  "馬龍"
];

function PlayerSelect(props) {
  const classes = useStyles();

  const [playerName, setPlayerName] = React.useState('');

  const handleChange = (event) => {
    setPlayerName(event.target.value);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label">選手</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          autoWidth
          value={playerName}
          onChange={handleChange}
        >
          {playerlist.map((player) => (
            <MenuItem key={player} value={player}>{player}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  )
}

GameTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GameTable);