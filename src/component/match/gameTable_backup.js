import PropTypes from 'prop-types';
import { withStyles, TableContainer, Paper, Table, TableHead, TableBody, TableRow, TableCell, IconButton, Collapse, Box, Grid, Button, InputBase } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import SearchIcon from '@material-ui/icons/Search'

const styles = theme => ({
  searchFieldStyle: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 150
  },
  tableStyle: {
    minWidth: 650,
  },
  inputStyle: {
    flex: 1,
  },
  iconButtonStyle: {
    padding: 10,
  },
  test: {
    
  }
});

function createData(uploadTime, gameTitle, players, gameResult) {
  return {uploadTime, gameTitle, players, gameResult};
}

const datas = [
  createData("2018/20/20 10:10:10", "2018德國公開賽", "樊振東 VS 馬龍", "4:2"),
  createData("2018/20/20 10:10:10", "2018德國公開賽", "樊振東 VS 馬龍", "1:2"),
  createData("2018/20/20 10:10:10", "2018德國公開賽", "樊振東 VS 馬龍", "5:6"),
]

class GameTable extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {classes} = this.props;

    return (
    <div>
      <TableContainer component={Paper}>
        <Table className={classes.tableStyle} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell>上傳時間</TableCell>
              <TableCell>比賽名稱</TableCell>
              <TableCell>選手</TableCell>
              <TableCell>結果</TableCell>
              <TableCell align='center'>
                <Paper component="form" className={classes.searchFieldStyle}>
                  <InputBase
                    className={classes.inputStyle}
                    placeholder="Search"
                    inputProps={{ 'aria-label': 'search' }}
                  />
                  <IconButton type="submit" className={classes.iconButtonStyle} aria-label="search">
                    <SearchIcon />
                  </IconButton>
                </Paper>
              </TableCell>
              <TableCell align='center'>
                <Button variant="outlined">
                  上傳影片
                </Button>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {datas.map((row) => (
              <Row key={row.gameResult} row={row} />
            ))}
            <TableRow><TableCell /></TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </div>
    );
  }
}

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
  test: {
    border: '5px solid red',
  }
});

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>

      <TableRow className={classes.rowStyle}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell onClick={() => {setOpen(!open)}} component="th" scope="row">{row.uploadTime}</TableCell>
        <TableCell onClick={() => {setOpen(!open)}}>{row.gameTitle}</TableCell>
        <TableCell onClick={() => {setOpen(!open)}}>{row.players}</TableCell>
        <TableCell onClick={() => {setOpen(!open)}}>{row.gameResult}</TableCell>
        <TableCell />
        <TableCell align='center'>
          <Grid container justify="space-around">
            <Button variant="outlined">標註</Button>
            <Button variant="outlined" color="secondary">刪除</Button>
          </Grid>
        </TableCell>
      </TableRow>

      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={7}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Grid container spacing={3} >
                <Grid container item xs={12} justify="flex-start" alignItems='center'>
                  <Grid item xs={1}>比賽名稱</Grid>
                  <Grid item xs={2} align='center'><Paper>2018德國公開賽</Paper></Grid>
                </Grid>

                <Grid container item xs={12}>
                  <Grid item xs={1}>選手</Grid>
                  <Grid item xs={1} align='center'><Paper>player1</Paper></Grid>
                  <Grid item xs={1} align='center'>VS</Grid>
                  <Grid item xs={1} align='center'><Paper>player2</Paper></Grid>
                </Grid>

                <Grid container item xs={12}>
                  <Grid item xs={1}>比分</Grid>
                  <Grid item xs={1} align='center'><Paper>5</Paper></Grid>
                  <Grid item xs={1} align='center'>:</Grid>
                  <Grid item xs={1} align='center'><Paper>4</Paper></Grid>
                </Grid>

                <Grid container item xs={12}>
                  <Grid item xs={1}>局分</Grid>
                  <Grid item xs={1} align='center'><Paper>11</Paper></Grid>
                  <Grid item xs={1} align='center'>:</Grid>
                  <Grid item xs={1} align='center'><Paper>9</Paper></Grid>
                </Grid>
              </Grid>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>

    </React.Fragment>
  )
}

GameTable.propTypes = {
  classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(GameTable);