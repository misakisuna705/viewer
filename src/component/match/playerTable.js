import PropTypes from 'prop-types';
import { withStyles, Button, Grid, Dialog, DialogTitle, DialogContent, DialogActions, TextField, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core/';
import MaterialTable from "material-table";

import AddBox from '@material-ui/icons/AddBox';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';


const styles = theme => ({
  dialog: {
    minHeight: '300px'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 360,
  }
});

class PlayerTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  handleOpen = () => {
    console.log("open");
    this.setState({open: true});
  }

  handleClose = () => {
    console.log("close");
    this.setState({open: false});
  }

  handleAddPlayer = () => {
    console.log("New Player");
    this.setState({open: false});
  }

  render() {
    const {classes} = this.props;
    const {open} = this.state;

    return (
      <div>
        <MaterialTable
          title="選手列表"
          columns={[
            { title: "上傳時間", field: "uploadTime" },
            { title: "選手名稱", field: "playerName" },
          ]}
          data={[
            {
              uploadTime: "2018/20/20 10:10:10",
              playerName: "樊振東",
            },
            {
              uploadTime: "2018/20/20 10:10:10",
              playerName: "馬龍",
            },
          ]}
          actions={[
            {
              icon: () => <AddBox />,
              tooltip: 'Add Player',
              isFreeAction: true,
              onClick: (event) => {this.handleOpen();}
            },
            {
              icon: () => <Edit />,
              tooltip: 'Edit player',
              onClick: (event, rowData) => {alert("Click " + rowData.uploadTime)}
            },
            {
              icon: () => <DeleteOutline />,
              tooltip: 'Delete player',
              onClick: (event, rowData) => {alert("Click " + rowData.uploadTime)}
            }
          ]}
          detailPanel={rowData => {
            return (
              <div
                style={{
                  fontSize: 100,
                  textAlign: 'center',
                  color: 'white',
                  backgroundColor: '#FDD835',
                }}
              >
                {rowData.playerName}
              </div>
            )
          }}
          options={{
            actionsColumnIndex: -1
          }}
        />
        <Dialog open={open} onClose={this.handleClose} maxWidth='xs'>
          <DialogTitle>新增選手資料</DialogTitle>
          <DialogContent>
            <Grid container spacing={4} className={classes.dialog}>
              <Grid item xs={12}>
                <TextField label="名稱" margin="dense" fullWidth/>
              </Grid>
              <Grid item xs={12}>
                <TextField label="身高" margin="dense" fullWidth/>
              </Grid>
              <Grid item xs={12}>
                <FormControl className={classes.formControl}>
                  <InputLabel>慣用手</InputLabel>
                  <Select
                    autoWidth
                  >
                    <MenuItem>左</MenuItem>
                    <MenuItem>右</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="secondary">
              Cancel
            </Button>
            <Button onClick={this.handleAddPlayer} color="primary">
              Add
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

PlayerTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PlayerTable);