export default class GUIController {

    constructor(renderer, camera, dragController, selectionController) {

        this.camera = camera
        this.dragController = dragController
        this.selectionController = selectionController

        this.gui = document.createElement('div')
        this.gui.style.position = 'fixed'
        this.gui.style.top = "35px"
        this.gui.style.left = "10px"
        this.gui.style.opacity = 0.85
        this.gui.style.zIndex = 10000
        this.gui.style.backgroundColor = 'black'
        this.gui.style.fontSize = "17px"
        this.gui.style.color = "white"
        
        const container = document.createElement('div')
        document.body.appendChild(container)
        container.appendChild(renderer.domElement)
        container.appendChild(this.gui)
    }
    
    update(){
        let x = this.camera.position.x.toFixed(1)
        let y = this.camera.position.y.toFixed(1)
        let z = this.camera.position.z.toFixed(1)
        this.gui.innerHTML = `camera pos (${x}, ${y}, ${z}) <br>`
        this.gui.innerHTML += `select ID: ${this.selectionController.IDselected} <br>`
        
        if(this.dragController)
        {
            x = this.dragController.mouse.x.toFixed(1)
            y = this.dragController.mouse.y.toFixed(1)
            z = this.dragController.mouse.z.toFixed(1)
            this.gui.innerHTML += `drag pos (${x}, ${y}, ${z}) <br> `
        }
    }

}