import {Dialog, List, ListItem, ListItemText, withStyles } from "@material-ui/core"

const useStyles = theme => ({
    root: {

    } 
});

class PlaybackDialogController extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {

        }
        this.selectedValue = this.props.selectedValue;
        this.onClose = this.props.onClose;

        this.handleListItemClick = this.handleListItemClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleListItemClick(rate) {
        this.onClose(rate);
    }

    handleClose() {
        this.onClose(this.selectedValue);
    }

    render() {
        const { classes, open } = this.props;
        return (
            <div>
                <Dialog fullWidth={true} maxWidth={'sm'} open={open} onClose={this.handleClose}>
                    <List>
                        <ListItem button onClick={() => this.handleListItemClick(0.1)}>
                            <ListItemText primary='0.1x' />
                        </ListItem>
                        <ListItem button onClick={() => this.handleListItemClick(0.5)}>
                            <ListItemText primary='0.5x' />
                        </ListItem>
                        <ListItem button onClick={() => this.handleListItemClick(1.0)}>
                            <ListItemText primary='1.0x' />
                        </ListItem>
                        <ListItem button onClick={() => this.handleListItemClick(2.0)}>
                            <ListItemText primary='2.0x' />
                        </ListItem>
                    </List>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(useStyles)(PlaybackDialogController);