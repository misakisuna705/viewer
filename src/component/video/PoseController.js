
import * as THREE from "three";
import { DragControls } from "./DragControls"
import PoseLoader from "./PoseLoader";
import SelectionController from "./SelectionController";

const humanColors = [
  0xcccccc,
  0xcccc33,
  0xcc33cc,
  0x33cccc,
  0xcc3333,
  0x33cc33,
  0x3333cc,

  0xcc9999,
  0xcc9933,
  0xcc3399,
  0x339999,
  0x993333,
  0x339933,
  0x333399,
];

  // --- poseMap structure ---
  // poseMap.get(frameID):
  //     humanID: {
  //       keypoints: [...],
  //       limcs: [keypoints pair ...]
  //     }
  //   }
  // 

  // --- Obj structure ---
  // - poseObj[humanID]: a poseGroup
  // - a poseGroup: a lot of kp and limbs
  //  
export default class PoseController {

  constructor(screenDimension, videoDimension, scene, layer) {

    this.screenDimension = screenDimension
    this.videoDimension = videoDimension
    this.frameID = -1

    this.layer = layer
    this.scene = scene

    // ------------
    // pooling
    // -------------

    const [kp_pool2D, draggables2D] = this.createKp2DPool(100);
    const [kp_pool3D, draggables3D] = this.createKp3DPool(100);
    const limb_pool = this.createLimbPool(200);
    
    // -------------
    // selection control
    // -------------
    const poseLoader = new PoseLoader()
    
    // ------------
    // set to "this"
    // ------------ 
    this.dragControls = []
    this.selectionControl = null
    
    this.kp_pool2D = kp_pool2D;
    this.kp_pool3D = kp_pool3D;
    this.limb_pool = limb_pool;

    this.draggables3D = draggables3D
    this.draggables2D = draggables2D
    
    this.poseLoader = poseLoader
  }

  // ---------------------
  // ----- API -----------
  // ---------------------

  loadPose(path, is2D_3D, isIK_raw, poseType)
  {
    if(this.poseMap)
    {
      this.poseMap.clear()
      delete this.poseMap
    }

    switch(poseType)
    {
      case "xnect":
        this.poseMap = this.poseLoader.loadXNectPose(path, is2D_3D, isIK_raw);
        break;
    }

    this.is2D_3D = is2D_3D
  }

  createDragControl(camera, renderer, controlID, cameraController=null)
  {
    const draggables = (this.is2D_3D == "2D")? this.draggables2D : this.draggables3D
    const dragControl = new DragControls(draggables, camera, renderer.domElement, this.layer);
    this.dragControls[controlID] = dragControl
    this.dragControls.mouse = {
      x: 0,
      y: 0,
      z: 0
    }   

    dragControl.addEventListener( 'dragstart', ( event ) => 
    {
      // ---- //
      const x = event.object.position.x
      const y = event.object.position.y
      const z = event.object.position.z
      console.log(`drag start: (${x}, ${y}, ${z})`)
      this.dragControls.mouse = {
        x: x,
        y: y,
        z: z
      }

      if(cameraController)
        cameraController.enabled = false;
    });

    dragControl.addEventListener( 'drag', ( event ) => 
    {
      // ---- //
      const x = event.object.position.x
      const y = event.object.position.y
      const z = event.object.position.z
      console.log(`dragging: (${x}, ${y}, ${z})`)
      this.dragControls.mouse = {
        x: x,
        y: y,
        z: z
      }
    });

    dragControl.addEventListener( 'dragend', ( event ) => 
    {
      // ------ //
      const x = event.object.position.x
      const y = event.object.position.y
      const z = event.object.position.z
      console.log(`drag start: (${x}, ${y}, ${z})`)
      this.dragControls.mouse = {
        x: x,
        y: y,
        z: z
      };
      if(cameraController)
        cameraController.enabled = true;

      // --- kp.name == kp_{3D/2D}_{humanID}_{jointID} ---//
      let name = event.object.name.split("_");
      let kp_or_limb = name[0];
      if(kp_or_limb == "kp")
      {
        let humanID = name[2];
        let jointID = name[3];
        let poseMap = undefined;
      
        if(name[1] == "3D")
        {
          // --- update kp in poseMap --- //
          poseMap = this.poseMap.get(this.frameID)

          poseMap[humanID].keypoints[jointID] = {
            cor_x: event.object.position.x,
            cor_y: event.object.position.y,
            cor_z: event.object.position.z,
          }

          // --- redraw the limbs --- //
          this.poseObj[humanID].traverse((limbObj)=>{
            let name = limbObj.name.split("_");
            if(name[0] == "limb")
            {
              let start = name[3];
              let end = name[4];
              if(start == jointID || end == jointID)
              {
                const keypoints = poseMap[humanID].keypoints;
                const points = [
                  new THREE.Vector3(keypoints[start].cor_x, keypoints[start].cor_y, keypoints[start].cor_z),
                  new THREE.Vector3(keypoints[end].cor_x, keypoints[end].cor_y, keypoints[start].cor_z)      
                ];
                limbObj.geometry.setFromPoints(points)
                limbObj.geometry.attributes.position.needsUpdate = true;
                limbObj.geometry.computeBoundingSphere();
              }
            }
          })
        } 
        else if (name[1] == "2D")
        {
          // --- update kp in poseMap --- //
          poseMap = this.poseMap.get(this.frameID)

          const original_x = (event.object.position.x + (this.screenDimension.width / 2)) 
          / this.screenDimension.width * this.videoDimension.width
          const original_y = (-event.object.position.y + (this.screenDimension.height / 2)) 
          / this.screenDimension.height * this.videoDimension.height
          poseMap[humanID].keypoints[jointID] = {
            cor_x: original_x,
            cor_y: original_y,
          }

          // --- redraw the limbs --- //
          this.poseObj[humanID].traverse((limbObj)=>{
            let name = limbObj.name.split("_");
            if(name[0] == "limb")
            {
              let start = name[3];
              let end = name[4];
              if(start == jointID || end == jointID)
              {
                const keypoints = poseMap[humanID].keypoints;
                const [startOffsetX, startOffsetY] = this.rescalePos(keypoints[start].cor_x, keypoints[start].cor_y)
                const [endOffsetX, endOffsetY]  = this.rescalePos(keypoints[end].cor_x, keypoints[end].cor_y)
                const points = [
                  new THREE.Vector3(startOffsetX, startOffsetY, this.layer),
                  new THREE.Vector3(endOffsetX, endOffsetY, this.layer)      
                ];
                limbObj.geometry.setFromPoints(points)
                limbObj.geometry.attributes.position.needsUpdate = true;
                limbObj.geometry.computeBoundingSphere();
              }
            }
          })
        }  
      }
    });
  }

  createSelectionControl(renderer)
  {
    const selectionControl = new SelectionController(humanColors)
    this.selectionControl = selectionControl

    renderer.domElement.addEventListener( 'keydown', ( event ) => 
    {
      if(event.keyCode == 90)
      {
        this.selectionControl.SelectNext(this.poseMap.get(this.frameID), this.poseObj)
      }
    });
  }

  updatePose(frame) {
    this.frameID = frame;
    const poseMap = this.poseMap.get(frame)

    // --- delete old pose --- //
    if (this.poseObj) 
    {
      for(let key in this.poseObj)
      {
        const element = this.poseObj[key]
        this.deletePose(element, this.scene);
      }
    }

    // --- create pose object --- //
    if (poseMap) 
    {
      this.poseObj = {};
      for(let humanID in poseMap)
      {
        const element = poseMap[humanID]
        if(this.is2D_3D == "2D")
          this.poseObj[humanID] = this.createPoseObject2D(element, humanID);
        else
          this.poseObj[humanID] = this.createPoseObject3D(element, humanID);
        this.scene.add(this.poseObj[humanID]);
      }
    } 
  }

  // ---------------------
  // Selection Control API
  // ---------------------

  removeSelectedID(removeFromNowOn)
  {
    if(!this.selectionControl)
    {
      console.error("no selection control")
      return
    }

    const pose = this.poseObj[this.selectionControl.IDselected]
    this.deletePose(pose)
    if(removeFromNowOn == "current")
      this.selectionControl.RemoveSelectedID(this.poseMap, this.frameID)
    else if(removeFromNowOn == "after")
      this.selectionControl.RemoveSelectedIDFromNowOn(this.poseMap, this.frameID)
    else if(removeFromNowOn == "all")
      this.selectionControl.RemoveSelectedIDAllFrame(this.poseMap)
    
    console.log(this.frameID)
    console.log(this.poseMap)
  }

  setNewID(newID)
  {
    if(!this.selectionControl)
    {
      console.error("no selection control")
      return
    }

    this.selectionControl.SetNewID(newID, this.poseMap, this.frameID, this.poseObj)
  }


  // ----------------------
  // Create Pooling
  // ---------------------

  createKp2DPool(numKP)
  {
    const pool = [];
    const draggables = []
    
    for(let i=0; i<numKP; i++)
    {
      const geometry = new THREE.CircleBufferGeometry(4);
      const material = new THREE.MeshBasicMaterial({
        color: 0xffffff,
      });
      const kp = new THREE.Mesh(geometry, material);
      pool.push(kp);
      draggables.push(kp);
    }

    return [pool, draggables];
  }

  createKp3DPool(numKP)
  {
    const pool = [];
    const draggables = []

    for(let i=0; i<numKP; i++)
    {
      const geometry = new THREE.SphereBufferGeometry(30, 10, 10);
      const material = new THREE.MeshBasicMaterial({
        color: 0xffffff,
      });
      const kp = new THREE.Mesh(geometry, material);
      pool.push(kp);
      draggables.push(kp);
    }

    return [pool, draggables];
  }

  createLimbPool(numLimb)
  {
    const pool = []

    for(let i=0; i<numLimb; i++)
    {
      const geometry = new THREE.BufferGeometry();
      const material = new THREE.LineBasicMaterial({
        color: 0xffffff,
      });
      const limb = new THREE.Line(geometry, material);
      pool.push(limb);
    }

    return pool;
  }

  
  // ------------------
  // create pose
  // ------------------
  
  createPoseObject2D(pose, humanID) {
    const keypoints = pose.keypoints;
    const limbs = pose.limbs;
    const poseGroup = new THREE.Group();

    if(keypoints.length == 0) 
      return poseGroup;

    // --- keypoints --- //
    keypoints.forEach((element, jointID) => {
      
      const [offsetX, offsetY] = this.rescalePos(element.cor_x, element.cor_y);

      // if(!this.kp_pool2D.length) 
      const kp = this.kp_pool2D.pop();
      kp.material.color.setHex(humanColors[humanID])

      kp.position.set(offsetX, offsetY, this.layer);
      kp.geometry.attributes.position.needsUpdate = true;
      kp.geometry.computeBoundingSphere();
      
      kp.layers.set(this.layer);
      kp.name = `kp_2D_${humanID}_${jointID}`

      poseGroup.add(kp);
    });

    // --- limbs --- //
    limbs.forEach((element) => {
      const start = element.start;
      const end = element.end;

      const [startOffsetX, startOffsetY] = this.rescalePos(keypoints[start].cor_x, keypoints[start].cor_y);
      const [endOffsetX, endOffsetY] = this.rescalePos(keypoints[end].cor_x, keypoints[end].cor_y);

      const points = [
        new THREE.Vector3(startOffsetX, startOffsetY, this.layer),
        new THREE.Vector3(endOffsetX, endOffsetY, this.layere),
      ];

      const limb = this.limb_pool.pop();
      limb.material.color.setHex(humanColors[humanID])
      
      // the last 2 lines prevent flickering
      limb.geometry.setFromPoints(points);
      limb.geometry.attributes.position.needsUpdate = true;
      limb.geometry.computeBoundingSphere();

      limb.layers.set(this.layer);
      limb.name = `limb_2D_${humanID}_${start}_${end}`

      poseGroup.add(limb);
    });

    return poseGroup;
  }

  createPoseObject3D(pose, humanID) {
    const keypoints = pose.keypoints;
    const limbs = pose.limbs;
    const poseGroup = new THREE.Group();

    if(keypoints.length == 0) 
      return poseGroup;

    // ---- keypoints ----- //
    keypoints.forEach((element, jointID) => {

      const kp = this.kp_pool3D.pop();
      kp.material.color.setHex(humanColors[humanID])

      kp.position.set(element.cor_x, element.cor_y, element.cor_z);
      kp.geometry.attributes.position.needsUpdate = true;
      kp.geometry.computeBoundingSphere();

      kp.layers.set(this.layer);
      kp.name = `kp_3D_${humanID}_${jointID}`

      poseGroup.add(kp);
    });

    // ------ limbs ------ //
    limbs.forEach((element) => {
      const start = element.start;
      const end = element.end;
      const points = [
        new THREE.Vector3(keypoints[start].cor_x, keypoints[start].cor_y, keypoints[start].cor_z),
        new THREE.Vector3(keypoints[end].cor_x, keypoints[end].cor_y, keypoints[end].cor_z),      
      ];

      const limb = this.limb_pool.pop();
      limb.material.color.setHex(humanColors[humanID])
      
      // the last 2 lines prevent flickering
      limb.geometry.setFromPoints(points);
      limb.geometry.attributes.position.needsUpdate = true;
      limb.geometry.computeBoundingSphere();

      limb.layers.set(this.layer);
      limb.name = `limb_3D_${humanID}_${start}_${end}`

      poseGroup.add(limb);
    });

    poseGroup.layers.set(this.layer)

    return poseGroup;
  }

  // ---------------------
  // ------ Utility ------ 
  // ---------------------


  deleteObject(obj) {
    if (obj) {
      this.scene.remove(obj);
      obj.geometry.dispose();
      obj.material.dispose();
      obj = undefined;
    }
  }

  deletePose(group) {
    if (group) {
      group.children.forEach((element) => {
        const name = element.name.split('_');
        
        // --- pooling
        if(name[0] == "kp")
        {
          if(name[1] == "2D")
            this.kp_pool2D.push(element);
          else if(name[1] == "3D")
            this.kp_pool3D.push(element);

          this.scene.remove(element);
        }
        else if(name[0] == "limb")
        {
          this.limb_pool.push(element);
          this.scene.remove(element);
        }
        // --- delete
        else
        {
          this.deleteObject(element);
        }

      });
      this.scene.remove(group);
      group = undefined;
    }
  }

  rescalePos(original_x, original_y)
  {
    const x = (original_x / this.videoDimension.width) * this.screenDimension.width - this.screenDimension.width / 2;
    const y = -((original_y / this.videoDimension.height) * this.screenDimension.height - this.screenDimension.height / 2);
    return [x, y];
  }

}