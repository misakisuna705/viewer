export default class PoseLoader {
  constructor() {
  }

  // poseMap structure
  // poseMap: {
  //   frameID: {
  //     humanID: {
  //       keypoints: [...],
  //       limbs: [keypoints pair ...]
  //     }
  //   }
  // }

  loadXNectPose(src, is_3D_2D, is_IK_raw) {
    const poseMap = new Map();
    fetch(src)
      .then((response) => response.text())
      .then((txt) => {
        let lines = txt.split("\n");

        // parse data per lien
        for (let i = 0; i < lines.length; i++) {

          // --- //
          const line = lines[i].split(" ");
          if(line.length <= 1) // empty line
            return
          const frameID = Number(line[0]);
          const humanID = line[1];
          const keypoints = [], limbs = [];
          if(is_3D_2D == "3D")  [keypoints, limbs] = this.XNectPose3D(line);
          else                  [keypoints, limbs] = this.XNectPose2D(line, is_IK_raw);

          // --- add to frame --- //
          if (!poseMap.has(frameID))
            poseMap.set(frameID,{});

          poseMap.get(frameID)[humanID] = 
          {
            keypoints: keypoints,
            limbs: limbs,
          }

        }
      })

      return poseMap;
  }

  XNectPose2D(line, is_IK_raw) {


    // 2D raw joints 
    // { 0:'head', 1:'neck',  2:'Rsho',  3:'Relb',  4:'Rwri',  5:'Lsho',  6:'Lelb', 7:'Lwri', ...
    //   8:'Rhip', 9:'Rkne', 10:'Rank', 11:'Lhip', 12:'Lkne', 13:'Lank' }

    const limb_pair_raw = [
      [0, 1], // head
      
      [1, 2],
      [2, 3],
      [3, 4], // R arm
      
      [1, 5],
      [5, 6], 
      [6, 7], // L arm
      
      [1, 8],
      [8, 9], 
      [9, 10], // R leg

      [1, 11], 
      [11, 12], 
      [12, 13], // L leg
    ];

    // 2D IK joints
    // { 0:'head TOP', 1:'neck',  2:'Rsho',  3:'Relb',  4:'Rwri',  5:'Lsho',  6:'Lelb', 7:'Lwri', ...
    //   8:'Rhip', 9:'Rkne', 10:'Rank', 11:'Lhip', 12:'Lkne', 13:'Lank', 14: Root, 15: Spine, 16:'Head', 17: 'Rhand', 18: 'LHand', 19: 'Rfoot', 20: 'Lfoot' }

    const limb_pair_IK = [
      [0, 16],
      [16, 1],
      [1, 15],
      [15, 14], // main torso

      [1, 2],
      [1, 5],
      [14, 8],
      [14, 11], // torso to limb

      [2, 3],
      [3, 4],
      [4, 17], // R arm

      [5, 6],
      [6, 7],
      [7, 18], // L arm

      [8, 9],
      [9, 10],
      [10, 19], // R leg

      [11, 12],
      [12, 13],
      [13, 20], // L leg
    ];


    // keypoints
    var numOfJoints = (is_IK_raw == "raw")? 14 : 21
    const keypoints = [];
    for (let i = 2; i < 2 + numOfJoints*2; i += 2) {
      let cor_x = Math.floor(line[i] * 2);
      let cor_y = Math.floor(line[i + 1] * 2);
      keypoints.push({
        cor_x: cor_x,
        cor_y: cor_y,
        color: this.rgb2hex([255, 255, 0]),
      });
    }

    // limbs
    const limbs = [];
    let limb_pair = (is_IK_raw=="IK")? limb_pair_IK : limb_pair_raw;
    for (let i = 0; i < limb_pair.length; i++) {
      limbs.push({
        start: limb_pair[i][0],
        end: limb_pair[i][1],
        color: this.rgb2hex([255, 255, 0]),
      })
    }

    return [keypoints, limbs];
  }

  XNectPose3D(line) {

    // 3D joints
    // { 0:'head TOP', 1:'neck',  2:'Rsho',  3:'Relb',  4:'Rwri',  5:'Lsho',  6:'Lelb', 7:'Lwri', ...
    //   8:'Rhip', 9:'Rkne', 10:'Rank', 11:'Lhip', 12:'Lkne', 13:'Lank', 14: Root, 15: Spine, 16:'Head', 17: 'Rhand', 18: 'LHand', 19: 'Rfoot', 20: 'Lfoot' }

    const limb_pair = [
      [0, 16],
      [16, 1],
      [1, 15],
      [15, 14], // main torso

      [1, 2],
      [1, 5],
      [14, 8],
      [14, 11], // torso to limb

      [2, 3],
      [3, 4],
      [4, 17], // R arm

      [5, 6],
      [6, 7],
      [7, 18], // L arm

      [8, 9],
      [9, 10],
      [10, 19], // R leg

      [11, 12],
      [12, 13],
      [13, 20], // L leg
    ];

    // keypoints
    const numOfJoints = 21
    const keypoints = [];
    for (let i = 2; i < 2 + numOfJoints*3; i += 3) {
      let cor_x = Math.floor(line[i]);
      let cor_y = Math.floor(line[i + 1]);
      let cor_z = Math.floor(line[i + 2]);
      keypoints.push({
        cor_x: cor_x,
        cor_y: cor_y,
        cor_z: cor_z,
        color: this.rgb2hex([255, 255, 0]),
      });
    }

    // limbs
    const limbs = [];
    for (let i = 0; i < limb_pair.length; i++) {
      limbs.push({
        start: limb_pair[i][0],
        end: limb_pair[i][1],
        color: this.rgb2hex([255, 255, 0]),
      })
    }

    return [keypoints, limbs];
  }

  rgb2hex(rgb) {
    return parseInt(
      rgb[0].toString(16) + rgb[1].toString(16) + rgb[2].toString(16),
      16
    );
  }
}
