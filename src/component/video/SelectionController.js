
export default class SelectionController {

    constructor(colors) {
        this.IDselected = null
        this.colors = colors
    }

    // ------------
    // API
    //-------------

    SelectNext(poseMap_curFrame, poseObj)
    {
        const oldSelectedID = this.IDselected;
        let next_is_to_select = (this.IDselected == null)? true : false
        let end_of_array = true
        
        // find next ID
        for(let humanID in poseMap_curFrame)
        {
            if(next_is_to_select)
            {
                this.IDselected = humanID
                end_of_array = false
                break
            }
            else if(humanID == this.IDselected)
            {
                next_is_to_select = true
            }
        }
        
        // if we have iterate through all human in the screen
        if(end_of_array)
        {
            this.IDselected = null
            this.changePoseColor(oldSelectedID, oldSelectedID, poseObj, false)
            return
        }
        
        // change the color of old/new pose
        console.log(`selectNext ${this.IDselected}`)
        this.changePoseColor(oldSelectedID, oldSelectedID, poseObj, false)
        this.changePoseColor(this.IDselected, this.IDselected, poseObj, true)
    }

    SetNewID(newID, poseMap, start_frameID, poseObj)
    {
        if(this.IDselected== null)
        {
            console.error("no ID selected")
            return
        }

        const oldID = this.IDselected
        this.IDselected = null
        this.changePoseColor(oldID, newID, poseObj, false)

        // change all index in poseMap "from now on"
        poseMap.forEach((curPoseMap, frameID)=>{
            if(curPoseMap[oldID] && frameID >= start_frameID )
            {
                curPoseMap[newID] = curPoseMap[oldID]
                delete curPoseMap[oldID]
            }
        })
        
    }

    RemoveSelectedID(poseMap, start_frameID)
    {
        if(this.IDselected== null)
        {
            console.error("no ID selected")
            return
        }
        delete poseMap.get(start_frameID)[this.IDselected]
        this.IDselected = null
    }

    RemoveSelectedIDFromNowOn(poseMap, start_frameID)
    {
        // ---- //
        if(this.IDselected== null)
        {
            console.error("no ID selected")
            return
        }

        // --- //
        poseMap.forEach((curPoseMap, frameID)=>{
            if(curPoseMap[this.IDselected] && frameID >= start_frameID )
            {
                delete curPoseMap[this.IDselected]
            }
        })
        this.IDselected = null
    }

    RemoveSelectedIDAllFrame(poseMap)
    {
        // ---- //
        if(this.IDselected== null)
        {
            console.error("no ID selected")
            return
        }

        // --- //
        poseMap.forEach((curPoseMap)=>{
            if(curPoseMap[this.IDselected])
            {
                delete curPoseMap[this.IDselected]
            }
        })
        this.IDselected = null
    }

    ReleaseSelectedID()
    {
        this.IDselected = null
    }

    // -----------
    // inner
    //-----------

    // change pose[oldID] to color of newID
    // and maybe lighten the color up if the pose is selected
    changePoseColor(oldID, newID, poseObj, selected)
    {
        const lighten = 1.2

        const pose = poseObj[oldID]
        const newColor = this.colors[newID]

        if(pose == undefined)
            return

        // iterate through all kp/limbs in poseObj
        pose.traverse((element)=>{
            if(element.material)
            {
                element.material.color.setHex(newColor)
                if(selected)
                {
                    element.material.color.multiplyScalar(lighten)
                }
            }
        })


    }


}
  