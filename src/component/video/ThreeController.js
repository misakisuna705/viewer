import { withStyles } from "@material-ui/core";
import * as THREE from "three";
import  PoseController  from "./PoseController"
import { OrbitControls } from "./OrbitControls";
import GUIController from "./GUIController";



const useStyles = (theme) => ({
  root: {},
});

// max 275
const layers = {
  hidden: 0,
  video: 1,
  court: 2,
  box: 3,
  demo: 4,
  pose2D: 5,
  pose3D: 6,
  blackPlane: 7
};

class ThreeController extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dimension: this.props.dimension,
      video: {
        dimension: this.props.videoDimension,
        vid: this.props.vid,
      },
      pose3DPath: {
        path: this.props.pose3DPath.path,
        is2D_3D: this.props.pose3DPath.is2D_3D,
        isIK_raw: this.props.pose3DPath.isIK_raw,
        poseType: this.props.pose3DPath.poseType,
      },
      pose2DPath: {
        path: this.props.pose2DPath.path,
        is2D_3D: this.props.pose2DPath.is2D_3D,
        isIK_raw: this.props.pose2DPath.isIK_raw,
        poseType: this.props.pose2DPath.poseType,
      },
      mode: "3D" // 2D or 3D
    };

    this.animate = this.animate.bind(this);
    this.renderScene = this.renderScene.bind(this);
  }

  componentDidMount() {
    
    const width = this.state.dimension.width;
    const height = this.state.dimension.height;
    
    // ------------
    // create renerer
    // ------------

    const renderer = new THREE.WebGLRenderer({
      antialias: true,
      alpha: true,
    });
    renderer.setSize(width, height);
    renderer.autoClear = false;
    
    // ------------
    // create scene
    // ------------
    
    const [scene2D, camera2D] = this.createScene2D(width, height, 60);
    const [scene3D, camera3D, camera3Dto2D] = this.createScene3D(width, height, 60);
    
    const video = this.state.video.vid;
    const [blackPlane, videoPlane] = this.createPlanes(width, height, video);
    scene2D.add(blackPlane)
    scene2D.add(videoPlane)

    // const box = this.createBox()
    // scene3D.add(box)

    // -----------
    // controllers
    // ------------

    const cameraControl = new OrbitControls (camera3D, renderer.domElement)
    cameraControl.keyPanSpeed = 60000;
    cameraControl.rotateSpeed = Math.PI / 20;
    cameraControl.screenSpacePanning = true;

    const pose3D = new PoseController (this.state.dimension, this.state.video.dimension, scene3D, layers.pose3D)
    pose3D.loadPose(this.state.pose3DPath.path, this.state.pose3DPath.is2D_3D, this.state.pose3DPath.isIK_raw, this.state.pose3DPath.poseType)
    pose3D.createDragControl(camera3D, renderer, layers.pose3D, 0, cameraControl)
    pose3D.createSelectionControl(renderer)
    
    // const pose2D = new PoseController (this.state.dimension, this.state.video.dimension, scene2D, layers.pose2D)
    // pose2D.loadPose(this.state.pose2DPath.path, this.state.pose2DPath.is2D_3D, this.state.pose2DPath.isIK_raw, this.state.pose2DPath.poseType)
    // pose2D.createDragControl(camera2D, renderer, layers.pose2D, 0, cameraControl)
    
    console.log(pose3D.poseMap)
    // console.log(pose2D.poseMap)

    // ------------
    // GUI
    // ------------

    const gui = new GUIController(renderer, camera3D, pose3D.dragControls, pose3D.selectionControl)
    this.gui = gui
    
    // ------------
    // set to "this"
    // ------------ 
    
    this.cameraControl = cameraControl;
    this.pose3D = pose3D;
    // this.pose2D = pose2D;

    this.scene2D = scene2D;
    this.scene3D = scene3D;

    this.camera2D = camera2D;
    this.camera3Dto2D = camera3Dto2D;
    this.camera3D = camera3D;

    this.renderer = renderer;
    this.threeRef.appendChild(this.renderer.domElement);

    this.clock = new THREE.Clock();

    this.toggleMode()
    this.animate();
  }

  // ---------------------------------------------- //
  // ---------- Create Scene & Objects ------------ //
  // ---------------------------------------------- //

  createScene2D(width, height, fov) {
    const scene = new THREE.Scene();
    
    // camera 2D
    const camera2D = new THREE.OrthographicCamera(
      width / -2,
      width / 2,
      height / 2,
      height / -2,
      1,
      1000
    );
    camera2D.position.set(0, 0, 275);
    camera2D.lookAt(new THREE.Vector3(0, 0, 0));

    return [scene, camera2D];
  }

  createScene3D(width, height, fov) {
    const scene = new THREE.Scene();

    const light = new THREE.DirectionalLight(0xffffff, 1.0);
    scene.add(light);

    // camera 3D
    const camera3D = new THREE.PerspectiveCamera(fov, width / height, 0.1, 10000);
    camera3D.position.set(0, 0, -1);
    camera3D.lookAt(new THREE.Vector3(0, 0, 1));

    // camera 3D to 2D
    const camera3Dto2D = new THREE.PerspectiveCamera(fov, width / height, 0.1, 10000);
    camera3Dto2D.position.set(0, 0, 0);
    camera3Dto2D.lookAt(new THREE.Vector3(0, 0, 1));
    camera3Dto2D.projectionMatrix.set(
      1.765517, 0,   -0.000089, 0, 
      0, 3.138697, -0.000089, 0,
      0, 0,       -1.00002, -0.200002,
      0, 0,       -1, 0
    );
    camera3Dto2D.projectionMatrixInverse.getInverse(camera3Dto2D.projectionMatrix);

    return [scene, camera3D, camera3Dto2D];
  }

  createPlanes(width, height, video) {
    // video
    const texture = new THREE.VideoTexture(video);
    const geometry = new THREE.PlaneBufferGeometry(width, height);
    const material = new THREE.MeshBasicMaterial({
      map: texture,
    });
    const videoPlane = new THREE.Mesh(geometry, material);
    videoPlane.layers.set(layers.video);
    
    // black
    const geometry2 = new THREE.PlaneBufferGeometry(width, height);
    const material2 = new THREE.MeshBasicMaterial({ 
      color: 0x000000,
      side: THREE.DoubleSide
    });
    const blackPlane = new THREE.Mesh(geometry2, material2);
    blackPlane.layers.set(layers.blackPlane);

    return [blackPlane, videoPlane]
  }

  createBox()
  {
    const box = new THREE.Mesh(
      new THREE.BoxBufferGeometry(),
      new THREE.MeshLambertMaterial( { color: 0xffff00 }
      ) );
    box.position.set(0, 0, 5)
    return box
  }

  // ---------------------------------------------- //
  // ------ get position / pose from outside ------ //
  // ---------------------------------------------- //

  updatePose(frame) {
    // do not change the order of these 2 line
    // or 2D pose cannot been draw(I don't know why)
    // this.pose2D.updatePose(frame);
    this.pose3D.updatePose(frame);
  }

  // ---------------------------------------------- //
  // --- Toggle: Control which layer is visible --- //
  // ---------------------------------------------- //

  toggleCourt() {
    this.camera3D.layers.toggle(layers.court);
  }

  toggle2D() {
    this.camera2D.layers.toggle(layers.box);
    this.camera2D.layers.toggle(layers.pose2D);
  }

  toggle3DPose() {
    this.camera3Dto2D.layers.toggle(layers.pose3D);
    this.camera3D.layers.toggle(layers.pose3D);
  }

  toggleMode()
  {
    this.camera2D.layers.disableAll();
    this.camera3D.layers.disableAll();
    this.camera3Dto2D.layers.disableAll();

    if(this.state.mode == "3D")
    {
      this.state.mode = "2D"
      this.camera2D.layers.enable(layers.video);
      this.camera2D.layers.enable(layers.pose2D);
      this.camera3Dto2D.layers.enable(layers.pose3D);
    }
    else
    {
      this.state.mode = "3D"
      this.camera2D.layers.enable(layers.blackPlane);
      this.camera3D.layers.enable(layers.pose3D);
    }
  }

  // ---------------------------------------------- //
  // -------------  Group control ----------------- //
  // ---------------------------------------------- //

  deleteObject(obj, scene) {
    if (obj) {
      scene.remove(obj);
      obj.geometry.dispose();
      obj.material.dispose();
      obj = undefined;
    }
  }

  deleteGroup(group, scene) {
    if (group) {
      group.children.forEach((element) => {
        this.deleteObject(element, this.scene2D);
      });

      scene.remove(group);
      group = undefined;
    }
  }
  
  // ---------------------------------------------- //
  // -------------  Render Scene ------------------ //
  // ---------------------------------------------- //

  // ??
  raycast(event) {
    const width = this.state.dimension.width;
    const height = this.state.dimension.height;
    const offset = this.threeRef.getBoundingClientRect();

    const mouseX = ((event.clientX - offset.x) / width) * 2 - 1;
    const mouseY = 1 - ((event.clientY - offset.y) / height) * 2;
    // console.log(mouseX, mouseY);

    const mouse = new THREE.Vector2(mouseX, mouseY);
    const raycaster = new THREE.Raycaster();

    raycaster.setFromCamera(mouse, this.camera3D);
    raycaster.layers.enableAll();

    //const intersect = raycaster.intersectObject(this.court);
    //console.log(intersect);
  }


  animate() {
    const delta = this.clock.getDelta();
    this.cameraControl.update(delta);
    this.gui.update()
    requestAnimationFrame(this.animate);
    this.renderScene();
  }

  renderScene() {
    this.renderer.clear();
    this.renderer.render(this.scene2D, this.camera2D);
    this.renderer.clearDepth();
    if(this.state.mode == "2D")
      this.renderer.render(this.scene3D, this.camera3Dto2D);
    else
      this.renderer.render(this.scene3D, this.camera3D);
  }

  render() {
    const { classes } = this.props;
    return (
      <div
        className={classes.root}
        ref={(ref) => {
          this.threeRef = ref;
        }}
        onClick={(event) => {
          this.raycast(event);
        }}
      ></div>
    );
  }
}

export default withStyles(useStyles)(ThreeController);
