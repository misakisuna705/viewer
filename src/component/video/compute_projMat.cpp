#include <stdio.h>

int main()
{
    // calibrated from img of dim (W, H)
    const float W = 1280, H = 720;
    const float near = 0.1, far = 10000;
    const float focal = 1130; 

    // output img dim offset
    const float x0 = 640, y0 = 360;

    float left = -W / 2;
    float right = W / 2;
    float bottom = -H / 2;
    float top = H / 2;

    left = left * near / focal - x0;
    right = right * near / focal - x0;
    bottom = bottom * near / focal - y0;
    top = top * near / focal - y0;

    float A = (right - left) / (right + left); 
    float B = (top - bottom) / (top + bottom); 
    float C = -(far + near) / (far - near);
    float D = -2*far*near / (far - near);

    float A00 = 2*near / (right - left); 
    float A11 = 2*near / (top - bottom); 

    printf("A: %f\n", A);
    printf("B: %f\n", B);
    printf("C: %f\n", C);
    printf("D: %f\n", D);

    printf("A00: %f\n", A00);
    printf("A11: %f\n", A11);


    return 0;
}