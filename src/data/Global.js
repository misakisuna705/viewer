import ApiCenter from "./apiCenter"
import DataCenter from "./dataCenter"

export default class Global {

    static name = "visual tool"
    static dataCenter = null;
    static apicenter = null;
    constructor() {

    }

    static get Instance() {
        if (this.apicenter === null) this.apicenter = new ApiCenter();
        if (this.dataCenter === null) this.dataCenter = new DataCenter();
        return this;
    }

    static get getGroupName() {
        return Global.dataCenter.currentGroupName
    }

    static get getLabelType() {
        return Global.dataCenter.currentLabelType
    }
}