import firebase from 'firebase';
import 'firebase/functions';
import axios from 'axios';

import Global from './Global';

firebase.initializeApp({
    apiKey: 'AIzaSyBpmyTejAm7GKKKhmlj7rELL6SEY2SD4wA',
    projectId: 'couthathena-tabletennis',
    storageBucket: "couthathena-tabletennis.appspot.com",
});


firebase.functions().useFunctionsEmulator("http://localhost:5001")
    // Getter
const getMatchList_fire = firebase.functions().httpsCallable('getMatchList')
const getPlayerList_fire = firebase.functions().httpsCallable('getPlayerList')
const getEventLabels_fire = firebase.functions().httpsCallable('getEventLabels')
    // Setter
const addMatchNode_fire = firebase.functions().httpsCallable('addMatchNode')
const addPlayerNode_fire = firebase.functions().httpsCallable('addPlayerNode')
const setEventLabels_fire = firebase.functions().httpsCallable('setEventLabels')
const makeFilePublic_fire = firebase.functions().httpsCallable('makeFilePublic')
    // const cloudfunctionUrl = "https://us-central1-couthathena-tabletennis.cloudfunctions.net/"

export default class ApiCenter {

    // deal with match cloud data sync and get
    getMatchList() {
        return new Promise(async(res, rej) => {
            console.log("get match list");

            try {
                let getMatchListResult = await getMatchList_fire({
                    group: Global.getGroupName,
                    type: Global.getLabelType
                })
                console.log(getMatchListResult)
                res(getMatchListResult);
            } catch (e) {
                console.warn(e)
                rej(e)
            }
        })

    }

    /**
     *  Add a new match data node to cloud 
     * @param {*} team1data team 1 data(name, members)
     * @param {*} team2data team 2 data(name, members)
     * @param {*} matchname matchname string
     * @param {*} video video file
     * @param {*} score score status
     */
    addMatch(team1data, team2data, matchname, video, score) {
        const group = Global.getGroupName //  TODO : edit
        return new Promise(async(res, rej) => {

            try {
                let addmatchResult = await addMatchNode_fire({
                    group: group,
                    type: Global.getLabelType,
                    team1: team1data,
                    team2: team2data,
                    matchname: matchname,
                    score: score
                })
                console.log(addmatchResult)
                const matchid = addmatchResult.data.matchid
                console.log(matchid)

                let storageRef = firebase.storage().ref();
                let uploadfileResult = await storageRef.child(`/${group}/videos/${matchid}.mp4`).put(video)
                console.log(uploadfileResult)
                let makepublicResult = await makeFilePublic_fire({
                    path: `${group}/videos/${matchid}.mp4`
                })
                console.log(makepublicResult)
                res(true)
            } catch (e) {
                console.warn(e)
                rej(false)
            }
        })
    };
    /**
     * save match data to assigned cloud node
     * @param {*} id 
     */
    saveMatch(id) {
        return new Promise((res, rej) => {
            console.log("Save match data");
            // TODO

        })
    };
    // player api

    /**
     * add player info to cloud 
     * @param {*} data 
     */
    addPlayerNode(data) { // TODO
        return new Promise(async(res, rej) => {
            console.log("Add player data");

            try {
                let addplayerResult = await addPlayerNode_fire({
                    group: Global.getGroupName,
                    type: Global.getLabelType,
                    playerdata: {
                        name: "testing"
                    }
                })
                console.log(addplayerResult);
                res(true)
            } catch (e) {
                console.warn(e)
                rej(false)
            }
        })
    };

    getPlayerList() {
        return new Promise(async(res, rej) => {
            console.log("Get player list data");
            try {
                let getPlayerListResult = await getPlayerList_fire({
                    group: Global.getGroupName,
                    type: Global.getLabelType
                })
                console.log(getPlayerListResult)
                res(getPlayerListResult);
            } catch (e) {
                console.warn(e)
                rej(e)
            }
        })
    };

    savePlayerInfo(id, data) {
        return new Promise(async(res, rej) => {
            try {

            } catch (e) {
                console.warn(e)
                rej(e)
            }
        })
    };
    //event api

    /**
     * add event to local data center
     * @param {*} data 
     */
    getEventLabels() {
        return new Promise(async(res, rej) => {
            try {
                let getEventResult = await getEventLabels_fire({
                    group: Global.getGroupName,
                    type: Global.getLabelType
                })
                console.log(getEventResult)
                res(getEventResult);
            } catch (e) {
                console.warn(e)
                rej(e)
            }
        })
    };

    setEventLabels() {
        return new Promise(async(res, rej) => {
            try {

            } catch (e) {
                console.warn(e)
                rej(e)
            }
        })
    }
}