import { withStyles } from "@material-ui/core/styles"
import Grid from '@material-ui/core/Grid';

import VideoController from "../component/VideoController";
import EventController from "../component/EventController";

const useStyles = theme => ({
  root: {
    flexGrow: 1,
  },
});

class LabelingPage extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="labelingPage">
        <EventController />
      </div>       
    );
  }
}

export default withStyles(useStyles)(LabelingPage);